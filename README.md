# PhyLinux build wrapper
This repository contains the phyLinux build wrapper for phytech's yocto Linux.

## Building with docker
Included in this repository is a simple docker wrapper to help building
without having to fulfill all build requirements on the host. The container
opens creates a build environment in the form of a shell.

> __Note:__ The wrapper actually (re-)builds the entire container locally as
> it is currently not stored (pushed) yet anywhere.

To start the docker build shell, a helper script is provided to ensure all
local files are available to the container. Calling `docker_build.sh` opens
the interactive build shell.
```sh
[user@host ~]$ ./docker_build.sh
...
buildbot@host:/workdir$
```

From within the container, the regular `phyLinux` flow can be used. For example
to initiate the setup for **phygate-tauri_imx6ul**
```sh
wget https://git.phytec.de/meta-pbacd20/plain/BSP-Yocto-IoT-phyGATE-Tauri-S-v0.1.xml
buildbot@host:/workdir$ phyLinux init -x BSP-Yocto-IoT-phyGATE-Tauri-S-v0.1.xml
```

From here on, the regular build instructions can be followed.

> __Note:__ A future version of phyLinux may include a way to clone the
> appropriate repositories first so that manually downloading the XML file may
> not be needed.
