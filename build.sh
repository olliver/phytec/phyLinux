#!/bin/sh

echo "To start a new clean build 'phyLinux clean && phyLinux init -x <xml>'"
echo "To continue '. sources/poky/oe-init-build-env'"

exec '/bin/bash'

exit 0
