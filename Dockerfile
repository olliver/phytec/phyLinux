# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

FROM index.docker.io/library/debian:stable-slim

LABEL maintainer="Olliver Schinagl <oliver@schinagl.nl>"

RUN \
    sed -i 's/main$/& contrib/g' '/etc/apt/sources.list' && \
    apt-get update && \
    apt-get install --no-install-recommends --yes \
        build-essential \
        ca-certificates \
        chrpath \
        cpio \
        diffstat \
        file \
        locales \
        gawk \
        git \
        python3 \
        python3-distutils \
        repo \
        sudo \
        wget \
    && \
    echo "en_US.UTF-8 UTF-8" >> '/etc/locale.gen' && \
    locale-gen && \
    ln -f -s '/usr/bin/python3' '/usr/local/bin/python' && \
    useradd --create-home buildbot && \
    echo "buildbot ALL=(ALL) NOPASSWD:ALL" > '/etc/sudoers.d/buildbot'

COPY "./phyLinux" "/usr/local/bin/"

USER buildbot

RUN \
    git config --global user.email 'bsp@pythec.de' && \
    git config --global user.name "Phytec BSP"

WORKDIR /workdir
